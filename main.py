from time import sleep
from os import getenv

from pyrogram import Client
from pyrogram.api.functions.messages import Search
from pyrogram.api.types import InputPeerSelf, InputMessagesFilterEmpty
from pyrogram.api.types.messages import ChannelMessages
from pyrogram.errors import FloodWait, UnknownError

# Add your API_ID and API_HASH from your telegram web profile here
API_ID = getenv('API_ID') or int(input('Enter your Telegram API ID: '))
API_HASH = getenv('API_HASH') or input('Enter your Telegram API HASH: ')

# Start Telegram Client
app = Client("client", API_ID, API_HASH)
app.start()

class Purge:
    # Init the purge, chat_id will be propted to the user later
    def __init__(self, peer=None, chat_id=None):
        self.peer = peer
        self.chat_id = chat_id
        self.message_ids = []
        self.add_offset = 0

    # Select the chat you want to purge yourself from
    def select_supergroup(self):
        dialogs = app.get_dialogs()
        groups = [x for x in dialogs if x.chat.type == 'supergroup']

        for i, group in enumerate(groups):
            print(f'{i+1}. {group.chat.title}')

        print('')

        group_n = int(input('Insert the number near the group name: '))
        selected_group = groups[group_n - 1]

        selected_group_peer = app.resolve_peer(selected_group.chat.id)
        self.peer = selected_group_peer
        self.chat_id = selected_group.chat.id

        print(f'You have selected: {selected_group.chat.title}\n')

        return selected_group, selected_group_peer

    # Start the purge
    def run(self):
        q = self.search_messages()
        self.update_ids(q)
        messages_count = q.count
        print(f'You have {messages_count} inside the selected group')

        if messages_count < 100:
            pass
        else:
            self.add_offset = 100

            for i in range(0, messages_count, 100):
                q = self.search_messages()
                self.update_ids(q)
                self.add_offset += 100

        self.delete_messages()

    @staticmethod
    def chunks(l, n):
        for i in range(0, len(l), n):
            yield l[i:i + n]

    # Update message IDS
    def update_ids(self, query: ChannelMessages):
        for msg in query.messages:
            self.message_ids.append(msg.id)

        return len(query.messages)

    # Core method, delete the message in sequence
    def delete_messages(self):
        print(f'Purge {len(self.message_ids)} messages. Next message IDs:')
        print(self.message_ids)
        for message_ids_chunk in self.chunks(self.message_ids, 100):
            try:
                app.delete_messages(chat_id=self.chat_id,
                                    message_ids=message_ids_chunk)
            except FloodWait as flood_exception:
                sleep(flood_exception.x)

    # Sarch for every user messages inside the group
    def search_messages(self):
        print(f'Searching messages. OFFSET: {self.add_offset}')
        return app.send(
            Search(
                peer=self.peer,
                q='',
                filter=InputMessagesFilterEmpty(),
                min_date=0,
                max_date=0,
                offset_id=0,
                add_offset=self.add_offset,
                limit=100,
                max_id=0,
                min_id=0,
                hash=0,
                from_id=InputPeerSelf()
            )
        )

# Main
if __name__ == '__main__':
    try:
        cleaner = Purge()
        cleaner.select_supergroup()
        cleaner.run()
    except UnknownError as e:
        print(f'Woops something went wrong: {e}')
        print('This could be caused by a change inside Telegram API')
    finally:
        app.stop()
